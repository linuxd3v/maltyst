
# Maltyst - mautic wordpress integration plugin

## Status:
Note  - this should be looked at as a still early project. Use with caution.

## Purpose:
Provide wordpress integration with mautic instance with majority of user interactions being done through wordpress site (using mautic api under the hood).

This may be preferred workflow for some consumers that prefer not to expose mautic instance and have relatively simple requirenments.

Highlights:
  
- **double optin functionality**  
User gets confirmation email and has to click to confirm enrollment in order to be added onto specific mautic segments. Confirmation url is on the wordress site.
  
- **throwaway emails detection and block**  
Emails from throwaway services like mailinator are not allowed.  This is not bulletprof and relies on third party library.
  
- **wordpress integrated preference center**  
  Preference center is also integrated into wordpress site.
    
- **wordpress integrated unsubscribe page** (same as preference center)  
  Preference center is also integrated into wordpress site.

- **"new posts" notifications**  
  You can designate a mautic segment, ex: "newposts" to receive new posts notifications once new posts change status from unpublished to published.

- **ajaxed fronend calls** 
  for improved UX and not delay a page render.

- subscribers added to mautic via api, never exposing mautic instance location.


## How to use:


1) add new custom field to mautic instance:  
`maltyst_contact_uqid  --- unique --- type=string`

Note: plugin will generate a unique token for each new subscriber. However - if you already have subscribers - you will have to generate unique tokens for them manually

2) Create wordpress page for preference-center && unsubscribe, for example: `/preference-center`  
And embed this shortcode:  `[maltyst_preference_center]`

3) Create wordpress page for double optin confirmation, ex: `/email-optin-result`
And embed this shortcode on there:  `[maltyst_optin_confirmation]`

5) Add optin form shortcode to sidebar or footer or wherever you like:
`[maltyst_optin_form]`

6) Create mautic segments you want to use.
    Note - preference center will only display segments explicitely marked to be displayed in preference center.

7) Create following emails in mautic with with whatever information you want users to see on initial email and on comfirmation email after user cliks on confirmation link. You have to use **replacement url formats** instead of mautic-provided tokens so users are directed to wordpress site instead:

* double-optin
* welcome

^ you can change template names in settings.




## Replacement URL Formats:
Note - as the goal is not to direct user to mautic instance, you shouldn't use tokens provided by mautic for **unsubscribe**, **preference center** or **double optin**. Rather use these instead:

Preference center:
```html
<a href="https://example.com/preference-center?maltyst_contact_uqid={contactfield=maltyst_contact_uqid}">Preference Center
</a>
```

Unsubscribe:
```html
<a href="https:/example.com/preference-center?maltyst_contact_uqid={contactfield=maltyst_contact_uqid}&unsubscribe-from-all=true">Unsubscribe
</a>
```

Double optin:
```html
<a href="{confirmation_url}={confirmation_token}">Let's do this</a>
```



## Example mautic segments:
  *  newposts - new posts notifications
  *  marketing - whatever 
  *  recurring


## Development:
- git checkout
- `composer install` - install backend deps (mautic api etc)
- `npm install` - install dependencies needed for public assets
- `npm run start` - combine/sass/minify/babel - etc.  typical gulp-ified frontend processing. 
- `npm run gen-dist` - generate a distributable plugin bundle


## Todo  

- make `double-optin` optional   
- **mjml** parsing at the email dispatch time. So going away with pre-compilation.
  This  way we can make mjml modifiable and expose in admin. Can probably solve this by implementing **mjml** api instead and requiring use to provide mjml token. 
- perhaps add optin captcha  
- perhaps add optin throttling  